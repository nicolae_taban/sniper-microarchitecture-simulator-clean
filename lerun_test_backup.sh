#!/bin/bash

SNIPER_INSTALATION_PATH=$(pwd)
BENCHMARKS_PATH=$SNIPER_INSTALATION_PATH/benchmarks
SNIPER_RESULTS_PATH=$(pwd)/results

export GRAPHITE_ROOT=$SNIPER_INSTALATION_PATH
export BENCHMARKS_ROOT=$BENCHMARKS_PATH
cd $BENCHMARKS_PATH
# make clean
# make

#./run-sniper -h
#./run-sniper -p splash2-fft -i test -n 4 -c gainestown -d $SNIPER_RESULTS_PATH/fft
#./run-sniper  -p splash2-lu.cont -i test -n 2 -c gainestown -d $SNIPER_RESULTS_PATH/lu_cont
#./run-sniper  -p splash2-fft -i small -n 4 -c gainestown -d $SNIPER_RESULTS_PATH/2
#./run-sniper  -p splash2-fft -i small -n 4 -c gainestown -d $SNIPER_RESULTS_PATH/3
#./run-sniper  -p splash2-fft -i small -n 4 -c gainestown -d $SNIPER_RESULTS_PATH/4
#./run-sniper  -p splash2-fft -i large -n 4 -c gainestown -d $SNIPER_RESULTS_PATH/100
#./run-sniper  -p splash2-fft -i small -n 4 -c gainestown -d $SNIPER_RESULTS_PATH/88

./run-sniper -senergystats -p splash2-barnes -i test -n 2 -c 0 --power -d $SNIPER_RESULTS_PATH/barnes &> $SNIPER_RESULTS_PATH/barnes/console_output.txt
#./run-sniper  -p splash2-cholesky -i test -n 4 -c gainestown -d $SNIPER_RESULTS_PATH/cholesky
#./run-sniper  -p splash2-fft -i test -n 4 -c gainestown -d $SNIPER_RESULTS_PATH/fft
#./run-sniper  -p splash2-fmm -i test -n 4 -c gainestown -d $SNIPER_RESULTS_PATH/fmm
#./run-sniper  -p splash2-lu.cont -i test -n 4 -c gainestown -d $SNIPER_RESULTS_PATH/lu_cont
#./run-sniper  -p splash2-lu.ncont -i test -n 4 -c gainestown -d $SNIPER_RESULTS_PATH/lu_ncont
#./run-sniper  -p splash2-ocean.cont -i test -n 4 -c gainestown -d $SNIPER_RESULTS_PATH/ocean_cont
#./run-sniper  -p splash2-ocean.ncont -i test -n 4 -c gainestown -d $SNIPER_RESULTS_PATH/ocean_ncont > $SNIPER_RESULTS_PATH/ocean_ncont/console_output.txt
#./run-sniper  -p splash2-ocean.ncont -i test -n 4 -c gainestown -d $SNIPER_RESULTS_PATH/ocean_ncont > $SNIPER_RESULTS_PATH/ocean_ncont/console_output.txt
#./run-sniper  -p splash2-radix -i test -n 4 -c gainestown -d $SNIPER_RESULTS_PATH/radix
#./run-sniper  -p splash2-raytrace -i test -n 4 -c gainestown -d $SNIPER_RESULTS_PATH/raytrace
#./run-sniper  -p splash2-volrend -i test -n 4 -c gainestown -d $SNIPER_RESULTS_PATH/volrend
#./run-sniper  -p splash2-water.nsq -i test -n 4 -c gainestown -d $SNIPER_RESULTS_PATH/water_nsq
#./run-sniper  -p splash2-water.sp -i test -n 4 -c gainestown -d $SNIPER_RESULTS_PATH/water_sp

#./run-sniper  -p local-pi -i test -n 2 -c gainestown -d $SNIPER_RESULTS_PATH/pi


cd $SNIPER_INSTALATION_PATH
